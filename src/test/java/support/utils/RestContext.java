package support.utils;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.Map;

public class RestContext {

    private static RequestSpecification resquest;
    private static Response response;

     public static void initRequest(){
         RestAssured.useRelaxedHTTPSValidation();
         resquest = RestAssured.given();
     }

    public static void setPath(String BaseUri, String path){
        if (resquest == null) {
            initRequest();
        }

        resquest.baseUri(BaseUri);
        resquest.basePath(path);

    }

    public static void setBody(String contentBoby) {
         resquest.body(contentBoby);
    }

    public static void getRequest() {
         response = resquest.get();
    }

    public static void postRequest() {
        response = resquest.post();
    }

    public static void deleteRequest() {
        response = resquest.delete();
    }

    public static void putRequest() {
        response = resquest.put();
    }

    public static void setPathParams(Map<String, String> params) {

         resquest.pathParams(params);
    }

    public static void setQueryParams(Map<String, String> params) {

        resquest.queryParams(params);
    }

    public static void setHeaders(Map<String, String> params) {

        resquest.headers(params);
    }

    public static Response getResponse() {
        return response;
    }

}
