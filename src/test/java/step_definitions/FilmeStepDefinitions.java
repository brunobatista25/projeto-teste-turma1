package step_definitions;

import com.github.javafaker.Faker;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import io.restassured.response.ValidatableResponse;
import model.Filme;
import org.json.JSONException;
import org.skyscreamer.jsonassert.JSONAssert;
import support.enums.ApiPath;
import support.enums.BaseUri;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static support.utils.RestContext.*;

public class FilmeStepDefinitions {
    ValidatableResponse response;

    Faker teste = new Faker();

    String nome = teste.name().firstName();
    String sinopse = teste.name().lastName();
    String faixaetaria = teste.chuckNorris().fact();
    String genero = teste.animal().name();
    Integer numero = teste.number().randomDigit();

    @Quando("faço uma requisicao para a url de filme")
    public void faço_uma_requisicao_para_a_url_de_filme() {
        initRequest();
        setPath(BaseUri.BASE_URI.getPath(), ApiPath.GET_FILMES.getPath());
        getRequest();
        response = getResponse().then().log().all();
    }
    @Então("valido se a resposta")
    public void valido_se_a_resposta() throws IOException, JSONException {
//        assertEquals(Integer.valueOf(1), response.extract().path("[0].codigo"));
//        assertEquals("TesteNome", response.extract().path("[0].nome"));
//        assertEquals("TesteSinopse", response.extract().path("[0].sinopse"));
//        assertEquals("14", response.extract().path("[0].faixaEtaria"));
//        assertEquals("TesteGenero", response.extract().path("[0].genero"));
//
//        assertEquals(Integer.valueOf(2), response.extract().path("[1].codigo"));
//        assertEquals("TesteNome2", response.extract().path("[1].nome"));
//        assertEquals("TesteSinopse2", response.extract().path("[1].sinopse"));
//        assertEquals("12", response.extract().path("[1].faixaEtaria"));
//        assertEquals("TesteGenero2", response.extract().path("[1].genero"));
//
//        assertEquals(Integer.valueOf(3), response.extract().path("[2].codigo"));
//        assertEquals("TesteNome3", response.extract().path("[2].nome"));
//        assertEquals("TesteSinopse3", response.extract().path("[2].sinopse"));
//        assertEquals("13", response.extract().path("[2].faixaEtaria"));
//        assertEquals("TesteGenero3", response.extract().path("[2].genero"));

        String jsonParaValidar = new String(Files.readAllBytes(Paths.get("src/test/resources/jsons/respostaFilmes.json")));

        JSONAssert.assertEquals(jsonParaValidar, response.extract().asString(), true);
    }


    @Quando("faço uma requisicao para a url de filme passando o codigo")
    public void faço_uma_requisicao_para_a_url_de_filme_passando_o_codigo() {
        initRequest();
        //http://localhost:8080/filme/{codigo}
        //http://localhost:8080/filme/1
        //http://localhost:8080/filme?TESTE=1234
        //http://localhost:8080/filme/5?content=json
        setPath(BaseUri.BASE_URI.getPath(), ApiPath.GET_FILME.getPath());
        Map<String, String> params = new HashMap<String , String>();
        params.put("codigo", "1");

        setPathParams(params);

        getRequest();
        response = getResponse().then().log().all();
    }
    @Então("valido se a resposta de um filme")
    public void valido_se_a_resposta_de_um_filme() {
        assertEquals(Integer.valueOf(1), response.extract().path("codigo"));
        assertEquals("TesteNome", response.extract().path("nome"));
        assertEquals("TesteSinopse", response.extract().path("sinopse"));
        assertEquals("14", response.extract().path("faixaEtaria"));
        assertEquals("TesteGenero", response.extract().path("genero"));
    }

    @Quando("faço uma requisicao para deletar um filme")
    public void faço_uma_requisicao_para_deletar_um_filme() {
        initRequest();
        setPath(BaseUri.BASE_URI.getPath(), ApiPath.DELETE_FILME.getPath());
        Map<String, String> params = new HashMap<String , String>();
        params.put("codigo", "3");

        setPathParams(params);

        deleteRequest();
        response = getResponse().then().log().all();
    }

    @Então("valido se o filme foi deletado")
    public void valido_se_o_filme_foi_deletado() {
        response.statusCode(200);
    }

    @Quando("faço uma requisicao para criar um filme")
    public void faço_uma_requisicao_para_criar_um_filme() throws IOException {
        initRequest();
        setPath(BaseUri.BASE_URI.getPath(), ApiPath.POST_CRIAR_FILME.getPath());
        Map<String, String> headers = new HashMap<String , String>();
        headers.put("Content-Type", "application/json");
        setHeaders(headers);
        //usando arquivo
        //String jsonParaValidar = new String(Files.readAllBytes(Paths.get("src/test/resources/jsons/respostaFilmes.json")));
        //setBody(jsonParaValidar);

        //passando o json direto no body
//        setBody("{\n" +
//                "  \"codigo\": 4,\n" +
//                "  \"nome\": \"TesteNome4\",\n" +
//                "  \"sinopse\": \"TesteSinopse4\",\n" +
//                "  \"faixaEtaria\": \"14\",\n" +
//                "  \"genero\": \"TesteGenero4\"\n" +
//                "}");


//        System.out.println(teste.gameOfThrones().character());

        Filme bodyObjeto = new Filme(numero, nome, sinopse, nome, genero);
        setBody(bodyObjeto.toString());
        System.out.println(bodyObjeto.toString());
        postRequest();
        response = getResponse().then().log().all();
    }

    @Então("valido se o filme foi criado")
    public void valido_se_o_filme_foi_criado() {
        assertEquals(numero, response.extract().path("codigo"));
        assertEquals(nome, response.extract().path("nome"));
        assertEquals(sinopse, response.extract().path("sinopse"));
        assertEquals(nome, response.extract().path("faixaEtaria"));
        assertEquals(genero, response.extract().path("genero"));
    }


    @Quando("faço uma requisicao para editar um filme")
    public void faço_uma_requisicao_para_editar_um_filme() {
        initRequest();
        setPath(BaseUri.BASE_URI.getPath(), ApiPath.EDITAR_FILME.getPath());
        Map<String, String> headers = new HashMap<String , String>();
        headers.put("Content-Type", "application/json");
        setHeaders(headers);
            // /FILME/{codigo} - > filme/1
        Map<String, String> params = new HashMap<String , String>();
        params.put("codigo", "1");

        setPathParams(params);

        Filme bodyObjeto = new Filme(1, nome, sinopse, nome, genero);
        setBody(bodyObjeto.toString());
        putRequest();

        response = getResponse().then().log().all();

    }
    @Então("valido se o filme foi editar")
    public void valido_se_o_filme_foi_editar() {
        assertEquals(Integer.valueOf(1), response.extract().path("codigo"));
        assertEquals(nome, response.extract().path("nome"));
        assertEquals(sinopse, response.extract().path("sinopse"));
        assertEquals(nome, response.extract().path("faixaEtaria"));
        assertEquals(genero, response.extract().path("genero"));
    }



}
