package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Filme {

    private Integer codigo;

    private String nome;

    private String sinopse;

    private String faixaEtaria;

    private String genero;

    public Filme(Integer codigo, String nome, String sinopse, String faixaEtaria, String genero) {
        this.codigo = codigo;
        this.nome = nome;
        this.sinopse = sinopse;
        this.faixaEtaria = faixaEtaria;
        this.genero = genero;
    }

    @Override
    public String toString() {
        return "  {\n" +
                "    \"codigo\":"+ codigo +",\n" +
                "    \"nome\": \"" + nome + "\",\n" +
                "    \"sinopse\": \"" + sinopse + "\",\n" +
                "    \"faixaEtaria\": \"" + faixaEtaria + "\",\n" +
                "    \"genero\": \"" + genero + "\"\n" +
                "  }";
    }

}
