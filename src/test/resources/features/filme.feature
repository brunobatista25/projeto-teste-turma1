#language: pt
Funcionalidade: Pegar Status do servidor

  Cenario: Buscar filme com sucesso
    Quando faço uma requisicao para a url de filme
    Então valido se a resposta

  Cenario: Buscar um filme pelo codigo
    Quando faço uma requisicao para a url de filme passando o codigo
    Então valido se a resposta de um filme

  Cenario: Deletar um filme com sucesso
    Quando faço uma requisicao para deletar um filme
    Então valido se o filme foi deletado

  @criar
  Cenario: Criar um filme com sucesso
    Quando faço uma requisicao para criar um filme
    Então valido se o filme foi criado

  @editar
  Cenario: Editar um filme com sucesso
    Quando faço uma requisicao para editar um filme
    Então valido se o filme foi editar